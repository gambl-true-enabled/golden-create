package com.golpain.paintgoldd

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import android.os.*
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.github.dhaval2404.colorpicker.MaterialColorPickerDialog
import com.github.dhaval2404.colorpicker.model.ColorShape
import kotlinx.android.synthetic.main.gold_paint_fragment.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.concurrent.CopyOnWriteArraySet


class GoldPaintFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.gold_paint_fragment, container, false)
    }

    private lateinit var surfView: DrawView
    private var paint = Paint()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        surfView = DrawView(context)
        gold_paint_container.addView(surfView)

        choose_color.setOnClickListener {
            MaterialColorPickerDialog
                .Builder(context!!)                    // Pass Activity Instance
                .setColorShape(ColorShape.SQAURE)   // Default ColorShape.CIRCLE
                .setDefaultColor(Color.WHITE)            // Pass Default Color
                .setColorListener { color, colorHex ->
                    paint.color = Color.parseColor(colorHex)
                    choose_color.setColorFilter(paint.color)
                }
                .show()
        }
        minus_stroke.setOnClickListener {
            if ((activity as MainActivity).strokeWidth > 9) (activity as MainActivity).strokeWidth -= 5
            width.text = (activity as MainActivity).strokeWidth.toInt().toString()
        }
        plus_stroke.setOnClickListener {
            (activity as MainActivity).strokeWidth += 5
            width.text = (activity as MainActivity).strokeWidth.toInt().toString()
        }
        save.setOnClickListener {

            val permissionStatus =
                ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            val permissionStatus1 =
                ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            if (permissionStatus == PackageManager.PERMISSION_GRANTED && permissionStatus1 == PackageManager.PERMISSION_GRANTED) {
                savePhoto(){}
            } else {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    PERMISSION_CODE_SAVE
                )
            }
        }
        share.setOnClickListener {
            val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
            val permissionStatus =
                ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            val permissionStatus1 =
                ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            if (permissionStatus == PackageManager.PERMISSION_GRANTED && permissionStatus1 == PackageManager.PERMISSION_GRANTED) {
                sharePhoto()
            } else {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    PERMISSION_CODE_SHARE
                )
            }
        }
    }

    private fun sharePhoto() {
        savePhoto() { uri ->
            shareImageUri(uri)
        }
    }

    private fun shareImageUri(uri: Uri) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.type = "image/png"
        startActivity(intent)
    }

    private fun savePhoto(callback: (uri: Uri) -> Unit) {
        var outStream: FileOutputStream? = null
        val folderMain = "GoldenCreate"

        val f = File(Environment.getExternalStorageDirectory(), folderMain)
        if (!f.exists()) {
            f.mkdirs()
        }
        val path: File =
            File(Environment.getExternalStorageDirectory()
                .toString() + "/GoldenCreate", "myZoomSnap${System.currentTimeMillis()}.png")
        try {
            outStream = FileOutputStream(path)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                usePixelCopy(surfView) {
                    it!!.compress(
                        CompressFormat.PNG,
                        90,
                        outStream
                    )
                    outStream.flush()
                    outStream.close()
                    Toast.makeText(
                        context!!,
                        "Picture is saved in folder GoldenCreate",
                        Toast.LENGTH_SHORT
                    ).show()
                    callback(Uri.fromFile(path))
                }
            } else {
                getBitmapFromView(surfView)!!.compress(
                    CompressFormat.PNG,
                    90,
                    outStream
                )
                outStream.flush()
                outStream.close()
                Toast.makeText(
                    context!!,
                    "Picture is saved in folder GoldenCreate",
                    Toast.LENGTH_SHORT
                ).show()
            }

        } catch (e: FileNotFoundException) {
            Log.d("CAMERA", e.message.toString())
        } catch (e: IOException) {
            Log.d("CAMERA", e.message.toString())
        }
    }

    fun getBitmapFromView(view: View): Bitmap? {
        val bitmap =
            Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun usePixelCopy(videoView: SurfaceView, callback: (Bitmap?) -> Unit) {
        val bitmap: Bitmap = Bitmap.createBitmap(
            videoView.width,
            videoView.height,
            Bitmap.Config.ARGB_8888
        );
        try {
            // Create a handler thread to offload the processing of the image.
            val handlerThread = HandlerThread("PixelCopier");
            handlerThread.start();
            PixelCopy.request(
                videoView, bitmap,
                PixelCopy.OnPixelCopyFinishedListener { copyResult ->
                    if (copyResult == PixelCopy.SUCCESS) {
                        callback(bitmap)
                    }
                    handlerThread.quitSafely();
                },
                Handler(handlerThread.looper)
            )
        } catch (e: IllegalArgumentException) {
            callback(null)
            // PixelCopy may throw IllegalArgumentException, make sure to handle it
            e.printStackTrace()
        }
    }

    fun grantedSave() {
        savePhoto(){}
    }

    fun grantedShare() {
        sharePhoto()
    }


    inner class DrawView(context: Context?) : SurfaceView(context),

        SurfaceHolder.Callback {

        private var drawThread: DrawThread? = null
        private var widthS = 0
        private var heightS = 0
        private var paints =
            CopyOnWriteArraySet<Pair<Paint, CopyOnWriteArraySet<Pair<Float, Float>>>>()


        override fun surfaceChanged(
            holder: SurfaceHolder, format: Int, width: Int,
            height: Int
        ) {
        }

        override fun surfaceCreated(holder: SurfaceHolder) {
            val metrics = context!!.resources.displayMetrics
            widthS = metrics.widthPixels
            heightS = metrics.heightPixels
            drawThread = DrawThread(getHolder())
            drawThread!!.setRunning(true)
            drawThread!!.start()
            paint.strokeWidth = (activity as MainActivity).strokeWidth
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            var retry = true
            drawThread!!.setRunning(false)
            while (retry) {
                try {
                    drawThread!!.join()
                    retry = false
                } catch (e: InterruptedException) {
                }
            }
        }

        private var line = CopyOnWriteArraySet<Pair<Float, Float>>()

        override fun onTouchEvent(event: MotionEvent?): Boolean {
            if (event!!.action == MotionEvent.ACTION_MOVE) {
                line.add(Pair(event.x, event.y))
            } else {
                if (event.action == MotionEvent.ACTION_UP && line.isNotEmpty()) {
                    val paintTemp = Paint()
                    paintTemp.color = paint.color
                    paintTemp.strokeWidth = paint.strokeWidth
                    paints.add(Pair(paintTemp, line))
                    line = CopyOnWriteArraySet<Pair<Float, Float>>()
                } else {
                }
            }
            return true

        }


        internal inner class DrawThread(private val surfaceHolder: SurfaceHolder) :
            Thread() {
            private var running = false
            fun setRunning(running: Boolean) {
                this.running = running
            }


            override fun run() {
                var canvas: Canvas?
                while (running) {
                    canvas = null
                    try {
                        canvas = surfaceHolder.lockCanvas(null)
                        if (canvas == null) continue
                        canvas.drawColor(Color.WHITE)
                        paint.strokeWidth = (activity as MainActivity).strokeWidth

                        paints.forEach {
                            val iterator = it.second.iterator()
                            var oldX = 0f
                            var oldY = 0f
                            var first = true
                            while (iterator.hasNext()) {
                                var next = iterator.next()
                                if (first) {
                                    first = false
                                } else {
                                    canvas.drawLine(
                                        oldX,
                                        oldY,
                                        next.first,
                                        next.second,
                                        it.first
                                    )
                                }
                                oldX = next.first
                                oldY = next.second
                            }

                        }

                        val iterator = line.iterator()
                        var oldX = 0f
                        var oldY = 0f
                        var first = true
                        while (iterator.hasNext()) {
                            var next = iterator.next()
                            if (first) {
                                first = false
                            } else {
                                canvas.drawLine(
                                    oldX,
                                    oldY,
                                    next.first,
                                    next.second,
                                    paint
                                )
                            }
                            oldX = next.first
                            oldY = next.second
                        }

                    } finally {
                        if (canvas != null) {
                            surfaceHolder.unlockCanvasAndPost(canvas)
                        }
                    }
                }
            }

        }

        init {
            holder.addCallback(this)
        }
    }

}