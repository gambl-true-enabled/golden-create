package com.golpain.paintgoldd

import android.content.pm.PackageManager
import android.media.AudioManager
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

const val PERMISSION_CODE_SAVE = 1217
const val PERMISSION_CODE_SHARE = 7125

class MainActivity : AppCompatActivity() {

    lateinit var goldPaintFragment: GoldPaintFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        goldPaintFragment = GoldPaintFragment()
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                goldPaintFragment
            ).commit()
    }

    var strokeWidth = 5f

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_VOLUME_UP -> {
                strokeWidth++
                true
            }
            KeyEvent.KEYCODE_VOLUME_DOWN -> {
                if (strokeWidth > 5) {
                    strokeWidth--
                }
                true
            }
            else -> false
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_CODE_SAVE){
            if (grantResults.contains(PackageManager.PERMISSION_DENIED)){

            }else{
                goldPaintFragment.grantedSave()
            }
        }else{
            if (requestCode == PERMISSION_CODE_SHARE){
                if (grantResults.contains(PackageManager.PERMISSION_DENIED)){

                }else{
                    goldPaintFragment.grantedShare()
                }
            }
        }
    }
}